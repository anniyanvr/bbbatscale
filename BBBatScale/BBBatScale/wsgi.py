import os
from django.core.wsgi import get_wsgi_application
from django.conf import settings

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'BBBatScale.settings.production')
application = get_wsgi_application()

if settings.DEBUG:
    try:
        import django.views.debug # noqa
    except ImportError:
        pass
