# Generated by Django 3.0.4 on 2020-03-14 22:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0003_auto_20200314_1824'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='jitsiinstance',
            name='disk',
        ),
        migrations.AddField(
            model_name='jitsiinstance',
            name='datacenter',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
        migrations.AlterField(
            model_name='jitsiinstance',
            name='ram',
            field=models.FloatField(default=0.5),
        ),
    ]
