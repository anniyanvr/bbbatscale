# Generated by Django 3.0.4 on 2020-03-24 10:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0021_auto_20200324_0943'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='room',
            name='is_password_public',
        ),
        migrations.AddField(
            model_name='room',
            name='alias',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
        migrations.AddField(
            model_name='room',
            name='building',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
    ]
