# Generated by Django 3.0.4 on 2020-04-08 09:53

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0033_generalparameter_greenlight_loadbalancer_url'),
    ]

    operations = [
        migrations.CreateModel(
            name='RoomConfiguration',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255, unique=True)),
                ('mute_on_start', models.BooleanField(default=True)),
                ('all_moderator', models.BooleanField(default=False)),
                ('everyone_can_start', models.BooleanField(default=False)),
                ('moderator_approval_needed', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='Tenant',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255, unique=True)),
            ],
        ),
        migrations.RemoveField(
            model_name='generalparameter',
            name='greenlight_loadbalancer_url',
        ),
        migrations.RemoveField(
            model_name='generalparameter',
            name='scalelite_secret',
        ),
        migrations.RemoveField(
            model_name='generalparameter',
            name='scalelite_url',
        ),
        migrations.RemoveField(
            model_name='instance',
            name='type',
        ),
        migrations.RemoveField(
            model_name='room',
            name='type',
        ),
        migrations.RemoveField(
            model_name='room',
            name='url_path',
        ),
        migrations.AddField(
            model_name='generalparameter',
            name='jitsi_url',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
        migrations.AddField(
            model_name='room',
            name='meeting_id',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
        migrations.AlterField(
            model_name='room',
            name='name',
            field=models.CharField(max_length=255, unique=True),
        ),
        migrations.AddField(
            model_name='instance',
            name='tenant',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='core.Tenant'),
        ),
        migrations.AddField(
            model_name='room',
            name='config',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='core.RoomConfiguration'),
        ),
        migrations.AddField(
            model_name='room',
            name='tenant',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='core.Tenant'),
        ),
    ]
