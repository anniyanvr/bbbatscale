# Generated by Django 3.0.4 on 2020-04-21 22:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0058_auto_20200419_2129'),
    ]

    operations = [
        migrations.AddField(
            model_name='tenant',
            name='token_registration',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
    ]
