# Generated by Django 3.0.4 on 2020-04-23 20:42

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0067_remove_room_instantiation_counter'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='room',
            name='user_config',
        ),
        migrations.AlterField(
            model_name='room',
            name='config',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='core.RoomConfiguration'),
        ),
    ]
