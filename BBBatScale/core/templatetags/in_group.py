from django import template
from django.contrib.auth.models import Group

register = template.Library()


@register.filter(name='in_group')
def in_group(user, group_name):
    if user.is_superuser:
        return True
    try:
        group = Group.objects.get(name=group_name)
        if group in user.groups.all():
            return True
    except Exception:
        return False
