from django.test import TestCase
from core.models import Room, Tenant, Instance
from core.constants import SCHEDULING_STRATEGY_LEAST_UTILIZATION, INSTANCE_STATE_UP


# Create your tests here.


class TenantTestCase(TestCase):

    def setUp(self):
        self.fbi = Tenant.objects.create(
            name="Fachbereich Informatik",
            description="Bilden Informatiker aus",
            notifications_emails="FBI Notification E-Mail",
            scheduling_strategy=SCHEDULING_STRATEGY_LEAST_UTILIZATION,
        )

        self.bbb_instance = Instance.objects.create(
            tenant=self.fbi,
            dns="bbb10.fbi.h-da.de",
            state=INSTANCE_STATE_UP,
            participant_count_max=10,
            videostream_count_max=2,
        )

        self.room_d19_304 = Room.objects.create(
            tenant=self.fbi,
            instance=self.bbb_instance,
            name="D19/304",
            participant_count=10,
            videostream_count=5,
        )

    def test__str__(self):
        c_tenant = Tenant(name="1337")
        self.assertEqual(c_tenant.__str__(), "1337")

    def test_get_current_participant_count(self):
        self.assertEqual(self.fbi.get_current_participant_count(), 10)

    def test_get_utilization(self):
        self.assertEqual(self.fbi.get_utilization(), 7)

    def test_get_utilization_max(self):
        self.assertEqual(self.fbi.get_utilization_max(), 2)

    def test_get_utilization_percent(self):
        self.assertEqual(self.fbi.get_utilization_percent(), 350)

    def test_get_instance_for_room(self):
        # alibi
        self.assertEqual(self.fbi.get_instance_for_room(), self.bbb_instance)

    def test_get_instances_up(self):
        # since there is only instance with the state of "instance state up"
        # the length of the queryset should be 1
        self.assertEqual(len((self.fbi.get_instances_up())), 1)


class InstanceTestCase(TestCase):

    def setUp(self):
        self.fbi = Tenant.objects.create(
            name="Fachbereich Informatik",
        )

        self.bbb_instance = Instance.objects.create(
            tenant=self.fbi,
            dns="bbb10.fbi.h-da.de",
            state=INSTANCE_STATE_UP,
            participant_count_max=10,
            videostream_count_max=2,
        )

        self.room_d19_304 = Room.objects.create(
            tenant=self.fbi,
            instance=self.bbb_instance,
            name="D19/304",
            participant_count=10,
            videostream_count=5,
        )

    def test__str__(self):
        self.assertEqual(self.bbb_instance.__str__(), "bbb10.fbi.h-da.de")

    def test_get_utilization(self):
        self.assertEqual(self.bbb_instance.get_utilization(), 7.0)

    def test_get_utilization_percent(self):
        self.assertEqual(self.bbb_instance.get_utilization_percent(), 350)

    def test_collect_stats(self):
        pass

    def test_get_participant_count(self):
        self.assertEqual(self.bbb_instance.get_participant_count(), 10)

    def test_get_videostream_count(self):
        self.assertEqual(self.bbb_instance.get_videostream_count(), 5)


class RoomTestCase(TestCase):

    def setUp(self):
        self.fbi = Tenant.objects.create(
            name="Fachbereich Informatik",
        )

        self.bbb_instance = Instance.objects.create(
            tenant=self.fbi,
            dns="bbb10.fbi.h-da.de",
            participant_count_max=10,
            videostream_count_max=2,
        )

        self.room_d19_304 = Room.objects.create(
            tenant=self.fbi,
            instance=self.bbb_instance,
            name="D19/304",
            participant_count=10,
            videostream_count=5,
        )

    def test__str__(self):
        self.assertEqual(self.room_d19_304.__str__(), "D19/304")

    def test_get_participants_current(self):
        self.assertEqual(self.room_d19_304.get_participants_current(), 10)

    def test_get_total_instantiation_counter(self):
        pass

    def test_is_meeting_running(self):
        pass

    def test_get_meeting_infos(self):
        self.assertIsNone(self.room_d19_304.get_meeting_infos())
