# Our Productive Setup 

The following describes the setup at our site at Darmstadt University of Applied Sciences (h_da) powered by BBB@Scale.

If you have any questions which are not answered by this document or [Getting Started Guide ](GettingStarted.md), feel free to send us a mail at mail@bbbatscale.de.
If you want send us an encrypted email, use the pgp-key under
([PGP-Key](mailAtBBBatScale.de_public.pgp)).

## Overview of the components

The following overview describes the BBB@Scale, BBB-Worker, BBB-Processing and BBB-Playback components involved in our h_da setup.
![hdarchitecture](/Documentation/hdaarchitecture.png?raw=true 'H_DA Architecture')

- BBB@Scale is deployed in our RedHat-Openshift-Container-Plattform (4PODs) running simultaneously 
- Postgres for BBB@Scale is also running in OCP
- HAProxy Load Balancers provide the entry point to BBB@Scale on OCP
- 69x BBB Server with [bbb-webhooks](https://docs.bigbluebutton.org/dev/webhooks.html) enabled
- 1x BBB Server exclusively for processing the recordings
- 1x BBB Server exclusively for playback of processed recordings

In the following you can find a brief system spec of our virtualized infrastructure followed by physical specs

- BBB-Worker and BBB-Playback Nodes (Virtual Resources)
    - 8 vCPU 
    - 16GB RAM
    - 100GB system disk
    - Ubuntu 16.04 as requested by bbb-install

- BBB-Processing Node (Virtual Resources)
    - 16 VCPU
    - 32GB RAM
    - 100GB system disk
    - 2x NVIDIA V100 GPGPU 

### Physical Hardware Resources used
For design and separation reasons we have implemented different tenants to get the resources from their data-centers separated in their own scheduling domain.

- Tenant1 provided 25x BBB-Worker Nodes
    - KVM-Based Virtualization / shared usage with other VMs
    - 151x Physical CPUs
    - 5TB RAM
- Tenant2 provided 20x BBB-Worker / 1x BBB-Processing
    - KVM-Based Virtualisation / exclusive usage
    - 184x Physical CPUs
    - 2TB RAM
    - 4x NVIDIA V100 GPGPUs
- Tenant3 provided 10 BBB-Worker Nodes 
    - VMWare-Based Virtualisation
- Tenant4 provided 10 BBB-Worker Nodes 
    - VMWare-Based Virtualisation
- Tenant5 (experimental) Offsite 10 BBB-Worker Nodes 
    - AWS XLarge
