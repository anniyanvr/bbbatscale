#!/usr/bin/env python

import subprocess
import sys
from time import sleep
from typing import Optional


def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, flush=True, **kwargs)


def await_postgres_connection(host: str, database: str, user: str,
                              attempts: Optional[int] = 5,
                              delay: int = 5) -> bool:
    counter = 0
    while subprocess.run(['pg_isready', '-h', host, '-d', database, '-U', user,
                          '-q']).returncode != 0:
        if attempts:
            if counter == attempts:
                return False
            counter += 1
        sleep(delay)
    return True


def main(*args: str):
    argc = len(args)
    if argc not in range(3, 5):
        eprint(f'Usage: {__file__} <host> <database> <user> [attempts=5] [delay=5]')
        eprint('  host: the host on which postgres is running')
        eprint('  database: the database to check if is ready')
        eprint('  user: the user to use to connect to the database')
        eprint('  attempts: if set to zero or more, it will only run \'attempts\'-times,'
               ' otherwise it will run indefinitely until a connection could be established')
        eprint('  delay: the delay in seconds to wait until the next attempt is made')
        sys.exit(2)

    arguments = [args[0], args[1], args[2]]
    if argc > 3:
        arguments.append(int(args[3]))
    if argc > 4:
        arguments.append(int(args[4]))
    if not await_postgres_connection(*arguments):
        eprint('Could not connect to the database.')
        sys.exit(1)


if __name__ == '__main__':
    main(*sys.argv[1:])
